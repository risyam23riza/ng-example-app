

export interface IBaseResponse<T> {
  response: T
}


export interface IEmployeeResponse<T> {
  currentPage: number,
  data: T,
  totalPages: number
  totalItems: number
}

export interface IEmployeeDetailResponse<T> {
  data: T
}
