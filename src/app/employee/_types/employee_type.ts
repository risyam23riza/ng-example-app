export interface IEmployee {
  username: string
  firstName: string
  lastName: string
  email: string
  birthDate: string
  basicSalary: number
  status: string
  group: string
  description: string
  position: number
}
