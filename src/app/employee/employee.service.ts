import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {IEmployee} from "./_types/employee_type";
import {BehaviorSubject, catchError, combineLatest, map, Observable, switchMap, tap, throwError} from "rxjs";
import {IBaseResponse, IEmployeeDetailResponse, IEmployeeResponse} from "./_types/custom_response.type";
import {generatePageNumber} from "../_utils/generate_page_number";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private employeeUrl = "/api/employees"
  private pageSubject$ = new BehaviorSubject<number>(1)
  page$ = this.pageSubject$.asObservable()
  private pageSizeSubject$ = new BehaviorSubject<number>(10)
  pageSize$ = this.pageSizeSubject$.asObservable()
  private collectionSizeSubject$ = new BehaviorSubject<number>(0)
  collectionSize$ = this.collectionSizeSubject$.asObservable()
  private sortFieldSubject$ = new BehaviorSubject<string>("")
  sortField$ = this.sortFieldSubject$.asObservable()
  private sortOrderSubject$ = new BehaviorSubject<string>("asc")
  sortOrder$ = this.sortOrderSubject$.asObservable()
  private searchQuerySubject = new BehaviorSubject<string>("")
  searchQuery$ = this.searchQuerySubject.asObservable()
  private searchQueryGroupSubject$ = new BehaviorSubject<string>("")
  searchQueryGroup$ = this.searchQueryGroupSubject$.asObservable()


  constructor(
    private http: HttpClient
  ) {

  }


  getEmployeeGroups() {
    return this.http.get<IBaseResponse<IEmployeeResponse<string[]>>>(`${this.employeeUrl}/groups?type=${this.searchQueryGroupSubject$.value}`).pipe(
      map(res => res.response.data)
    )
  }

  createEmployee(body: any) {
    return this.http.post(this.employeeUrl, body).pipe()
  }

  getEmployeeById(id: string | null) {
    return this.http.get<IBaseResponse<IEmployeeDetailResponse<IEmployee[]>>>(`${this.employeeUrl}/${id}`).pipe(
      map((res) => res.response.data[0])
    )
  }

  employeesList$() {
    const pageNumbers = generatePageNumber(this.pageSubject$.value, this.pageSizeSubject$.value)
    console.log('Debug 🐞 ==> Class: EmployeeService, Function: employeesList$, Line 56 pageNumbers(): '
      , '\n', pageNumbers);
    return this.http.get<IBaseResponse<IEmployeeResponse<IEmployee[]>>>(`${this.employeeUrl}?page=${this.pageSubject$.value}&limit=${this.pageSizeSubject$.value}&sortField=${this.sortFieldSubject$.value}&sortOrder=${this.sortOrderSubject$.value}&searchQuery=${this.searchQuerySubject.value}`).pipe(
      map(res => {
        const transform = res.response.data.map((value, index) => {
          this.setCollectionSizeSubject(res.response.totalItems)
          return {...value, position: pageNumbers[index]}
        })

        console.log('Debug 🐞 ==> Class: EmployeeService, Function: , Line 65 (): '
          , '\n', transform);

        return {...res, data: transform}.response.data
      }))
  }

  employeesCombine$() {
    return combineLatest([
      this.page$,
      this.pageSize$,
      this.sortField$,
      this.sortOrder$,
      this.searchQuery$
    ]).pipe(
      switchMap(([page, pageSize]) => {
        return this.employeesList$()
      }),
      catchError(this.handleError)
    )
  }

  employeeGroupsCombine$ = combineLatest([
    this.searchQueryGroup$
  ]).pipe(
    switchMap(() => {
      return this.getEmployeeGroups()
    })
  )

  setPageSubject(page: number) {
    this.pageSubject$.next(page)
  }

  setPageSizeSubject(pageSize: number) {
    this.pageSizeSubject$.next(pageSize)
  }

  setCollectionSizeSubject(size: number) {
    this.collectionSizeSubject$.next(size)
  }

  setSortFieldSubject(orderField: string) {
    this.sortFieldSubject$.next(orderField)
  }

  setSortOrderSubject(sortOrder: string) {
    this.sortOrderSubject$.next(sortOrder)
  }

  setSearchQuerySubject(searchQuery: string) {
    this.searchQuerySubject.next(searchQuery)
  }

  setSearchQueryGroupSubject(searchQuery: string) {
    this.searchQueryGroupSubject$.next(searchQuery)
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message
      }`;
    }
    console.error(errorMessage);
    return throwError(() => errorMessage);
  }
}
