import {AbstractControl, ValidatorFn} from "@angular/forms";


export class EmployeeValidators {
  static basicSalaryValidation(control: AbstractControl) {
    const value = control.value
    if (isNaN(value)) {
      return { invalidBasicSalary: true };
    }
    return null;
  }
}
