import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {EmployeeListComponent} from './_pages/employee-list/employee-list.component';
import {EmployeeCreateComponent} from './_pages/employee-create/employee-create.component';
import {EmployeeDetailComponent} from './_pages/employee-detail/employee-detail.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {ReactiveFormsModule} from "@angular/forms";
import {MatSortModule} from "@angular/material/sort";
import {MatCardModule} from "@angular/material/card";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatButtonModule} from "@angular/material/button";
import {NumericInputDirective} from "../directives/numeric-input.directive";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {PipesModule} from "../_pipes/pipes.module";


const routes: Routes = [
  {
    path: '',
    component: EmployeeListComponent
  },
  {
    path: 'create',
    component: EmployeeCreateComponent
  },
  {
    path: ':id',
    component: EmployeeDetailComponent
  }
]

@NgModule({
  declarations: [
    EmployeeListComponent,
    EmployeeCreateComponent,
    EmployeeDetailComponent,
    NumericInputDirective,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule, MatInputModule, MatSelectModule,
    MatDatepickerModule, MatNativeDateModule,
    ReactiveFormsModule, MatButtonModule, MatSnackBarModule, PipesModule
  ]
})
export class EmployeeModule {
}
