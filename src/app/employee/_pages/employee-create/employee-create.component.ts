import {AfterViewInit, ChangeDetectionStrategy, Component, inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EmployeeValidators} from "../../employee-form-validation";
import {MatSnackBar} from "@angular/material/snack-bar";
import {EmployeeService} from "../../employee.service";
import {Router} from "@angular/router";
import {combineLatest, debounceTime, distinctUntilChanged, fromEvent, map, tap} from "rxjs";

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeCreateComponent implements OnInit, AfterViewInit {

  employeeCreateForm: FormGroup
  // datepicker field
  maxDate = new Date()
  private fb = inject(FormBuilder)
  private _snackBar = inject(MatSnackBar)
  private _employeeService = inject(EmployeeService)
  employeeGroups$ = combineLatest([
    this._employeeService.employeeGroupsCombine$
  ]).pipe(
    map(([group]) => group)
  )
  private _router = inject(Router)

  constructor() {

  }

  ngOnInit() {
    this.initForm()
  }

  initForm() {
    this.employeeCreateForm = this.fb.group({
      "username": ["", [Validators.required]],
      "firstName": ["", [Validators.required]],
      "lastName": ["", [Validators.required]],
      "email": ["", [Validators.required, Validators.email]],
      "birthDate": ["", [Validators.required]],
      "basicSalary": ["", [Validators.required, EmployeeValidators.basicSalaryValidation]],
      "status": ["", [Validators.required]],
      "group": ["", [Validators.required]],
      "description": ["", [Validators.required]],
      "searchQueryGroup": [""]
    } )
  }

  ngAfterViewInit() {
    this.employeeCreateForm.get('searchQueryGroup')?.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap((query) => {
        console.log('query', query)
        return this._employeeService.setSearchQueryGroupSubject(query)
      })
    ).subscribe()


  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      verticalPosition: 'top',
      horizontalPosition: 'end'
    });
  }

  onSubmit() {
    if (this.employeeCreateForm.invalid) this.openSnackBar("something wen't wrong", "close")

    this._employeeService.createEmployee({...this.employeeCreateForm.value}).subscribe()

  }

  async onGoBack() {
    await this._router.navigate(['/employees'])
  }


}
