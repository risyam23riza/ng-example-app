import {Component, inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EMPTY, Observable, of, switchMap} from "rxjs";
import {EmployeeService} from "../../employee.service";
import {Location} from "@angular/common";
import {IEmployee} from "../../_types/employee_type";

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit{

  private _route = inject(ActivatedRoute)
  private _router = inject(Router)
  private _location = inject(Location)
  private _employeeService = inject(EmployeeService)

  employee$: Observable<IEmployee>

  constructor() {
    this.employee$ = of({} as IEmployee)
  }
  ngOnInit() {
    this.employee$ = this._route.paramMap.pipe(
      switchMap((res) => {
        if(res.has('id')) {
          return this._employeeService.getEmployeeById(res.get('id')) as Observable<IEmployee>
        }
        return EMPTY
      })
    )
  }

  async onGoEmployeeList() {


      await this._router.navigate(['/employees'])



  }

}
