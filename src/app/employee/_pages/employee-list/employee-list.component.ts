import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  inject,
  OnInit, TemplateRef,
  ViewChild
} from '@angular/core';
import {EmployeeService} from "../../employee.service";
import {IEmployee} from "../../_types/employee_type";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {FormBuilder, FormGroup} from "@angular/forms";
import {combineLatest, debounceTime, distinctUntilChanged, filter, map, tap} from "rxjs";
import {MatSort, Sort} from "@angular/material/sort";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material/snack-bar";
import {Router} from "@angular/router";


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeListComponent implements OnInit, AfterViewInit {

  employeeListForm: FormGroup
  dataSource: MatTableDataSource<IEmployee>
  employees: Array<IEmployee>
  searchQuery: string = ""
  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort
  private fb = inject(FormBuilder)
  private _router = inject(Router)
  private _snackBar = inject(MatSnackBar)

  private employeeService = inject(EmployeeService)
  employeeMaster$ = combineLatest([
    this.employeeService.page$,
    this.employeeService.pageSize$,
    this.employeeService.collectionSize$,
    this.employeeService.employeesCombine$()
  ]).pipe(
    map(([page, pageSize, collectionSize, employees]) => ({
      page, pageSize, collectionSize, employees
    }))
  )
  private cdr = inject(ChangeDetectorRef)


  @ViewChild('snackBarTemplate') snackBarRef: TemplateRef<any>

  constructor() {
    this.employees = []
    this.dataSource = new MatTableDataSource<IEmployee>([] as IEmployee[])
    this.employeeListForm = this.fb.group({
      searchQuery: [""]
    })
  }

  get columnsToDisplay() {
    return [
      "position",
      "username",
      "firstName",
      "lastName",
      "email",
      "birthDate",
      "basicSalary",
      "status",
      "group",
      "description",
      "actions"

    ]
  }

  ngAfterViewInit() {


    this.employeeListForm.get('searchQuery')?.valueChanges.pipe(
      filter(Boolean),
      debounceTime(500),
      distinctUntilChanged(),
      tap((res) => {
        console.log('Debug 🐞 ==> Class: EmployeeListComponent, Function: , Line 87 (): '
          , '\n', res);
        this.employeeService.setSearchQuerySubject(res)
      })
    ).subscribe()

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSnackBar(message: string, action: string, config?: MatSnackBarConfig) {

        return this._snackBar.open(message, action, {
          ...config,
          verticalPosition: 'top',
          horizontalPosition: 'end'
        });


  }

  ngOnInit() {

    this.employeeMaster$.pipe(
      map(({employees}) => {
        return employees
      })
    ).subscribe({
      next: (res) => {
        this.dataSource = new MatTableDataSource<IEmployee>(res)
      }
    })

  }


  handlePageChange(page: PageEvent) {

    if (page.pageIndex === 1) this.employeeService.setPageSubject(page.pageIndex + 1)
    if(page.pageIndex <= 0) this.employeeService.setPageSubject(1)
    if (page.pageIndex > 1) this.employeeService.setPageSubject(page.pageIndex)
    this.employeeService.setPageSizeSubject(page.pageSize)

    console.log('Debug 🐞 ==> Class: EmployeeListComponent, Function: handlePageChange, Line 110 page(): '
      , '\n', page);

  }

  generatePageSizeOptions(start: number, end: number) {
    const pageSizeOptions: number[] = [];

    for (let pageSize = start; pageSize <= end; pageSize += start) {
      pageSizeOptions.push(pageSize);
    }

    return pageSizeOptions;
  }

  /** Announce the change in sort state for assistive technology. */
  announceSortChange(sortState: Sort) {
    const {active, direction} = sortState

    this.employeeService.setSortFieldSubject(active)
    this.employeeService.setSortOrderSubject(direction)


  }

  async onGoEmployeeDetail(id: string) {
    const stateData = {
      page: 1,
    };

    await this._router.navigate([`/employees/${id}`], {state: stateData})
  }

  onDeleteEmployee(employee: IEmployee) {
    this.openSnackBar(`Example success delete record ${employee.username}`, "close", {
      panelClass: ['custom-snackbar-delete'],
    })


    // this._snackBar.open(`Dummy message ${employee.username} has been deleted.`, "close", {
    //   panelClass: ['custom-snackbar'],
    //   verticalPosition: 'top',
    //   horizontalPosition: 'end'
    // })

  }

  onEditEmployee(employee: IEmployee) {
    this.openSnackBar(`Example success update record ${employee.username}`, "close", {
      panelClass: ['custom-snackbar-edit'],
    })
  }

}
