
export const generatePageNumber = (page: number, pageSize: number) => {
  const startIndex = ((page - 1) * pageSize) < 0 ? 0 : (page - 1) * pageSize
  const endIndex = startIndex + pageSize

  const pageNumbers = []
  for(let i = startIndex + 1; i <= endIndex; i++) {
    pageNumbers.push(i)
  }

  return pageNumbers
}
