import {Response, Server} from "miragejs"
import { saveAs } from 'file-saver';

import {EmployeeDummyData} from "../assets/dummy_employee_data"
import {generatePageNumber} from "./_utils/generate_page_number";


const employees = EmployeeDummyData['employees']

export default () => {
  new Server({
    seeds(server) {
      server.db.loadData({employees})
    },

    routes() {
      this.namespace = "/api"

      this.post('/employees', (schema, request) => {
        const body = JSON.parse(request.requestBody)
        const blob = new Blob([JSON.stringify(body)], { type: 'application/json;charset=utf-8' });
        saveAs(blob, 'output.json');
        return {
          response: {
            message: "Success"
          }
        }
      })

      this.get('/employees/groups', (schema, request) => {
        const {type} = request.queryParams
        const groups = ['Sales & Marketing', 'HRD (Human Resources Department)', "Purchasing", "Production", "QA (Quality Assurance)", "Accounting",
          "Warehouse", "IT (Information & Technology)", "PPIC (Product Planning Inventory Control)", "GA (General Affairs)"]
        let filteredGroups = [...groups];

        if(type) {
          filteredGroups = filteredGroups.filter((group) => {
            return group.trim().toLowerCase().includes(type.trim().toLowerCase())
          });
        }


        return {
          response: {
            data: filteredGroups
          }
        }
      })

      this.get('/employees', (schema, request) => {
        // Define a function to paginate, sort, and search employee data


        function filterEmployees(page: number, limit: number, sortField: string, sortOrder: string, searchQuery: string) {
          let filteredEmployees = [...schema.db['employees']];

          // Search
          if (searchQuery) {
            const searchRegex = new RegExp(searchQuery, 'i');
            filteredEmployees = filteredEmployees.filter((employee) => {
              const employeeValues = Object.values<string>(employee);
              const matches = employeeValues.some(value => {
                const isMatch = searchRegex.test(value);
                console.log(`Search: ${searchQuery}, Value: ${value}, Match: ${isMatch}`);
                return isMatch;
              });
              return matches;
            });
          }

          // Sort
          if (sortField) {
            filteredEmployees.sort((a, b) => {
              if (a[sortField] < b[sortField]) return sortOrder === 'asc' ? -1 : 1;
              if (a[sortField] > b[sortField]) return sortOrder === 'asc' ? 1 : -1;
              return 0;
            });
          }

          // Paginate
          const startIndex = (page - 1) * limit
          const endIndex = startIndex + limit;
          return filteredEmployees.slice(startIndex, endIndex);
        }

        try {
          let {page, limit, sortField, sortOrder, searchQuery} = request.queryParams
          const paginatedEmployees = filterEmployees(
            +page,
            +limit,
            sortField,
            sortOrder,
            searchQuery
          );

          const pageNumbers = generatePageNumber(+page, +limit)

          return {
            response: {
              data: paginatedEmployees.map((res,index) => ({...res, position: pageNumbers[index]})),
              currentPage: +page,
              totalPages: Math.ceil(schema.db['employees'].length / +limit),
              totalItems: searchQuery ? paginatedEmployees.length : schema.db['employees'].length
            }

          }
        } catch (error: any) {
          return new Response(
            400,
            {},
            {errors: ["something wen't wrong"]}
          )
        }

      })

      this.get('employees/:id', (schema, request) => {
        const {id} = request.params
        const data = schema.db['employees'].filter(res => Number(res.id) === Number(id))
        return {
          response: {
            data
          }
        }
      })
    }
  })
}
