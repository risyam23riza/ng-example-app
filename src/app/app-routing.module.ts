import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SigninComponent} from "./auth/signin/signin.component";

const routes: Routes = [
  {
    path: 'employees', loadChildren: () => import("./employee/employee.module").then(m => m.EmployeeModule)
  },
  {
    path: 'login', component: SigninComponent, pathMatch: 'full'
  },
  {
    path: '', redirectTo: "/login", pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
