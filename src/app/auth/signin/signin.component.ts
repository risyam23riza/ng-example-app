import {Component, inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent {


  private _fb = inject(FormBuilder)
  private _router = inject(Router)

  authLoginForm: FormGroup

  constructor() {
    this.authLoginForm = this._fb.group({
      email: ["example@email.com", [Validators.required,Validators.email]],
      password: ["password", [Validators.required,Validators.minLength(6)]]
    })
  }

  onSubmit() {
  //   NO validation
    this._router.navigate(['/employees'])


  }

}
